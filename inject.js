setTimeout(()=>{
    //Убирает это назойливое окно подтверждения если мы открывали дискорда в неактивной вкладке
    if (document.querySelector('button[type="submit"]') != null) document.querySelector('button[type="submit"]').click()
    //Открываем эксперементальные настройки discord
    let script = document.createElement('script')
    script.src = chrome.runtime.getURL('experiments.js')
    document.documentElement.appendChild(script)
    script.onload = function() {
        script.remove()
    }
}, 10000)

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.command === 'headphones') {
        document.querySelector('button[aria-label="Откл. звук"]').click()
    } else if (request.command === 'microphone') {
        document.querySelector('button[aria-label="Откл. микрофон"]').click()
    }
})

window.onbeforeunload = () => {return 'Не ухади'}